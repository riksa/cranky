package org.riksa.cranky;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import com.facebook.*;
import com.facebook.model.GraphUser;
import com.google.inject.Inject;
import com.stackmob.sdk.callback.StackMobCallback;
import com.stackmob.sdk.callback.StackMobModelCallback;
import com.stackmob.sdk.exception.StackMobException;
import org.riksa.cranky.model.User;
import org.riksa.cranky.services.IdentityService;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;
import roboguice.util.Ln;

import java.util.concurrent.CountDownLatch;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends RoboActivity {
    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello",
            "bar@example.com:world"
    };

    /**
     * The default email to populate the email field with.
     */
    public static final String EXTRA_EMAIL = "com.example.android.authenticatordemo.extra.EMAIL";

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // Values for email and password at the time of the login attempt.
    private String mEmail;
    private String mPassword;

    // UI references.
    @InjectView(R.id.username)
    private EditText mUsernameView;

    @InjectView(R.id.password)
    private EditText mPasswordView;

    @InjectView(R.id.login_form)
    private View mLoginFormView;

    @InjectView(R.id.login_status)
    private View mLoginStatusView;

    @InjectView(R.id.login_status_message)
    private TextView mLoginStatusMessageView;

    @Inject
    IdentityService identityService;

    private UiLifecycleHelper uiHelper;

    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {
            Ln.d("Logged in...token=%s", session.getAccessToken());
//            Request request = Request.newGraphPathRequest(null, "/4", new Request.Callback() {
//                @Override
//                public void onCompleted(Response response) {
//                    if (response.getError() != null) {
//                        Log.i("MainActivity", String.format("Error making request: %s", response.getError()));
//                    } else {
//                        GraphUser user = response.getGraphObjectAs(GraphUser.class);
//                        Log.i("MainActivity", String.format("Name: %s", user.getName()));
//                        mUsernameView.setText(user.getName());
//                    }
//                }
//            });
//            request.executeAsync();

            final User newUser = new User();
            newUser.loginWithFacebook(session.getAccessToken(), new StackMobModelCallback() {
                @Override
                public void success() {
                    // Logged in, let's go
                    identityService.setUser(newUser);
                    Intent intent = new Intent(LoginActivity.this, CrankyActivity.class);
                    startActivity(intent);
                    finish(); // finish this activity so that back key won't take us back to login screen
                }

                @Override
                public void failure(StackMobException e) {
                    try {
                        Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
                            @Override
                            public void onCompleted(GraphUser user, Response response) {
                                if (response.getError() != null) {
                                    Ln.d("Error making request: %s", response.getError());
                                } else {
                                    Ln.d("Name: %s", user.getName());
                                    mUsernameView.setText(user.getName());
                                    Intent intent = new Intent(LoginActivity.this, NewUserActivity.class);
                                    intent.putExtra(NewUserActivity.FULL_NAME, user.getName());
                                    startActivity(intent);
                                }
                            }
                        });
                        request.executeAsync();
                    } catch (Exception e1) {
                    }
                }
            });


        } else if (state.isClosed()) {
            Ln.d("Logged out...");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        // Set up the login form.
        mEmail = getIntent().getStringExtra(EXTRA_EMAIL);
        mUsernameView.setText(mEmail);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
        Settings.addLoggingBehavior(LoggingBehavior.REQUESTS);
//
//        Request request = Request.newGraphPathRequest(null, "/4", new Request.Callback() {
//            @Override
//            public void onCompleted(Response response) {
//                if (response.getError() != null) {
//                    Log.i("MainActivity", String.format("Error making request: %s", response.getError()));
//                } else {
//                    GraphUser user = response.getGraphObjectAs(GraphUser.class);
//                    Log.i("MainActivity", String.format("Name: %s", user.getName()));
//                }
//            }
//        });
//        request.executeAsync();

        uiHelper = new UiLifecycleHelper(this, callback);
        uiHelper.onCreate(savedInstanceState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        mEmail = mUsernameView.getText().toString();
        mPassword = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password.
        if (TextUtils.isEmpty(mPassword)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (mPassword.length() < 4) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(mEmail)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
//        } else if (!mEmail.contains("@")) {
//            mUsernameView.setError(getString(R.string.error_invalid_email));
//            focusView = mUsernameView;
//            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
            showProgress(true);
            mAuthTask = new UserLoginTask();
            mAuthTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginStatusView.setVisibility(View.VISIBLE);
            mLoginStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            mLoginFormView.setVisibility(View.VISIBLE);
            mLoginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... params) {
            final CountDownLatch countDownLatch = new CountDownLatch(1);

            final User user = new User(mEmail, mPassword);
            user.login(new StackMobCallback() {
                @Override
                public void success(String s) {
                    // Logged in with credentials
                    countDownLatch.countDown();
                }

                @Override
                public void failure(StackMobException e) {
                    // Failed to login. Let's see if the user exists...
                    user.fetch(new StackMobCallback() {
                        @Override
                        public void success(String s) {
                            // user exists -> wrong password
                            countDownLatch.countDown();
                        }

                        @Override
                        public void failure(StackMobException e) {
                            // user did not exists. Create...
                            Intent intent = new Intent(LoginActivity.this, NewUserActivity.class);
                            intent.putExtra(NewUserActivity.FULL_NAME, mEmail);
                            startActivity(intent);
                            countDownLatch.countDown();
//
//                            user.save(new StackMobCallback() {
//                                @Override
//                                public void success(String s) {
//                                    user.login(new StackMobCallback() {
//                                        @Override
//                                        public void success(String s) {
//                                            countDownLatch.countDown();
//                                        }
//
//                                        @Override
//                                        public void failure(StackMobException e) {
//                                            countDownLatch.countDown();
//                                        }
//                                    });
//                                }
//
//                                @Override
//                                public void failure(StackMobException e) {
//                                    countDownLatch.countDown();
//                                }
//                            });
                        }
                    });
                }
            });

            try {
                countDownLatch.await();
            } catch (InterruptedException e) {
                Ln.e(e.getMessage(), e);
            }
            return user.isLoggedIn();
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                Intent intent = new Intent(LoginActivity.this, CrankyActivity.class);
                startActivity(intent);
                finish(); // finish this activity so that back key won't take us back to login screen
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}
