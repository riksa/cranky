package org.riksa.cranky.abs;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;
import org.riksa.cranky.CrankySurfaceView;
import org.riksa.cranky.R;
import org.riksa.cranky.services.CareerService;
import org.riksa.cranky.services.HardwareService;
import org.riksa.cranky.services.IdentityService;
import org.riksa.twirl.TwirlCallback;
import org.riksa.twirl.TwirlListener;
import roboguice.inject.InjectView;

import javax.annotation.Nullable;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/11/13
 */
public class CrankTabFragment extends RoboSherlockFragment implements TwirlCallback {
    public static final String TAG = "crank";

    final Map<Integer, Double> twirlAngles = new HashMap<Integer, Double>();
    final Map<Integer, Integer> twirlRotations = new HashMap<Integer, Integer>();

    @Nullable
    @InjectView(tag = "score")
    TextView tv_score;

    @Nullable
    @InjectView(tag = "local_score")
    TextView tv_local_score;

    @Nullable
    @InjectView(tag = "identity")
    TextView tv_identity;

    @Inject
    CareerService careerService;

    @Inject
    IdentityService identityService;

    @Inject
    HardwareService hardwareService;

    private int score = 0;
    private double localScore;
    private CrankySurfaceView surfaceView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_crank, container, false);
        surfaceView = (CrankySurfaceView) view.findViewById(R.id.surfaceview);

        ViewGroup dragListener = (ViewGroup) view.findViewWithTag("drag_listener");
        TwirlListener twirlListener = new TwirlListener(dragListener);
//        twirlListener.setDrawTwirlers(BuildConfig.DEBUG);
        twirlListener.setTwirlCallback(this);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        score = careerService.getScore();
        localScore = 0;
        updateStats();
    }

    @Override
    public void onPause() {
        super.onPause();
        careerService.setScore((int) (careerService.getScore() + localScore));
    }

    @Override
    public void twirlUpdate(int pointerId, Rect rect, double handleDistance, double angle, int rotations, double localAngle) {
        twirlAngles.put(pointerId, angle);
        twirlRotations.put(pointerId, rotations);

        double totalRotations = 0d;
        for (Integer twirlRotation : twirlRotations.values()) {
            totalRotations += twirlRotation;
        }
        localScore = totalRotations;

        updateStats();
        surfaceView.twirlUpdate(pointerId, rect, handleDistance, angle, rotations, localAngle);
        hardwareService.hapticVibrate(angle);

    }

    private void updateStats() {
        if (tv_identity != null) {
            tv_identity.setText(identityService.getUser().getUsername());
        }

        if (tv_score != null) {
            tv_score.setText(NumberFormat.getInstance().format(score + localScore));
        }

        if (tv_local_score != null) {
            tv_local_score.setText(NumberFormat.getInstance().format(localScore));
        }
    }

    @Override
    public void twirlRemoved(int pointerId, double angle, int rotations, double localAngle) {
        surfaceView.twirlRemoved(pointerId, angle, rotations, localAngle);
    }

    @Override
    public void twirlAdded(int pointerId, double angle, int rotations, double localAngle) {
        surfaceView.twirlAdded(pointerId, angle, rotations, localAngle);
    }
}