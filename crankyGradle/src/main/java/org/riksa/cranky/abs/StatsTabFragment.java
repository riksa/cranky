package org.riksa.cranky.abs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.github.rtyley.android.sherlock.roboguice.fragment.RoboSherlockFragment;
import com.google.inject.Inject;
import org.riksa.cranky.R;
import org.riksa.cranky.model.User;
import org.riksa.cranky.services.AsyncCallback;
import org.riksa.cranky.services.IdentityService;
import org.riksa.cranky.services.StatisticsService;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/11/13
 */
public class StatsTabFragment extends RoboSherlockFragment {
    public static final String TAG = "stats";
    private static final String KEY_SCORE = "score";
    private static final String KEY_NAME = "name";

    @Inject
    IdentityService identityService;

    @Inject
    StatisticsService statisticsService;

    TextView tv_username;
    TextView tv_userscore;
    ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab_stats, container, false);

        tv_username = (TextView) view.findViewById(R.id.tv_username);

        tv_userscore = (TextView) view.findViewById(R.id.tv_userscore);

        listView = (ListView) view.findViewById(R.id.list);

        updateContent();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Who needs lambdas!
        statisticsService.refresh(new AsyncCallback() {
            @Override
            public void success() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        updateContent();
                    }
                });
            }

            @Override
            public void failure() {

            }
        });
    }

    private void updateContent() {
        User me = identityService.getUser();
        tv_username.setText(me.getUsername());
        tv_userscore.setText(NumberFormat.getInstance().format(me.getScore()));

        List<User> leaderBoard = statisticsService.getLeaderBoard();
        List<Map<String, String>> data = new LinkedList<Map<String, String>>();
        for (User user : leaderBoard) {
            Map<String, String> map = new HashMap<String, String>();
            map.put(KEY_NAME, user.getUsername());
            map.put(KEY_SCORE, NumberFormat.getInstance().format(user.getScore()));
            data.add(map);
        }

        SimpleAdapter adapter = new SimpleAdapter(
                getActivity(),
                data,
                R.layout.leaderboard_list_item,
                new String[]{KEY_NAME, KEY_SCORE},
                new int[]{android.R.id.text1, android.R.id.text2});
        listView.setAdapter(adapter);

    }

}