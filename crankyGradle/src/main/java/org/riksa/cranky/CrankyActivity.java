package org.riksa.cranky;

import android.os.Bundle;
import com.actionbarsherlock.app.ActionBar;
import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import org.riksa.cranky.abs.CrankTabFragment;
import org.riksa.cranky.abs.StatsTabFragment;
import org.riksa.cranky.abs.TabListener;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/4/13
 */
public class CrankyActivity extends RoboSherlockFragmentActivity {
    public static String ACTIVE_TAB = "activeTab";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(org.riksa.cranky.R.layout.activity_crank);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        // add tabs
        ActionBar.Tab tab1 = actionBar.newTab()
                .setText(org.riksa.cranky.R.string.tab_title_drill)
                .setTabListener(new TabListener<CrankTabFragment>(
                        this, CrankTabFragment.TAG, CrankTabFragment.class));

        ActionBar.Tab tab2 = actionBar.newTab()
                .setText(org.riksa.cranky.R.string.tab_title_stats)
                .setTabListener(new TabListener<StatsTabFragment>(
                        this, StatsTabFragment.TAG, StatsTabFragment.class));

        actionBar.addTab(tab1);
        actionBar.addTab(tab2);

        // check if there is a saved state to select active tab
        if (savedInstanceState != null) {
            getSupportActionBar().setSelectedNavigationItem(
                    savedInstanceState.getInt(ACTIVE_TAB));
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // save active tab
        outState.putInt(ACTIVE_TAB, getSupportActionBar().getSelectedNavigationIndex());
        super.onSaveInstanceState(outState);
    }
}