package org.riksa.cranky.model;

import com.stackmob.sdk.model.StackMobUser;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/12/13
 */
public class User extends StackMobUser {
    private int score;

    public User(String username, String password) {
        super(User.class, username, password);
    }

    public User(String username) {
        super(User.class, username);
    }

    public User() {
        super(User.class);
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
