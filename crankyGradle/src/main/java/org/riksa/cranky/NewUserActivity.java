package org.riksa.cranky;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.google.inject.Inject;
import com.stackmob.sdk.callback.StackMobCallback;
import com.stackmob.sdk.exception.StackMobException;
import org.riksa.cranky.model.User;
import org.riksa.cranky.services.IdentityService;
import roboguice.activity.RoboActivity;
import roboguice.inject.InjectView;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/17/13
 */
public class NewUserActivity extends RoboActivity {
    public static final String FULL_NAME = "full_name";

    @InjectView(tag = "username")
    EditText editTextUsername;

    @InjectView(tag = "password")
    EditText editTextPassword;

    @InjectView(tag = "button_create")
    Button button;

    @InjectView(R.id.login_form)
    private View mLoginFormView;

    @InjectView(R.id.login_status)
    private View mLoginStatusView;

    @Inject
    IdentityService identityService;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(org.riksa.cranky.R.layout.activity_new_user);

        editTextUsername.setText(getIntent().getStringExtra(FULL_NAME));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                createUser();

            }
        });
    }

    private void createUser() {
        String facebookToken = identityService.getFacebookToken();
        if (TextUtils.isEmpty(facebookToken)) {
            createUserWithCredentials();
        } else {
            createUserWithFacebook();
        }
    }

    private void createUserWithCredentials() {
        final User user = new User(editTextUsername.getText().toString(), editTextPassword.getText().toString());
        user.save(new StackMobCallback() {
            @Override
            public void success(String s) {
                identityService.setUser(user);
                Intent intent = new Intent(NewUserActivity.this, CrankyActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void failure(StackMobException e) {
                editTextUsername.setError(getString(org.riksa.cranky.R.string.username_already_in_use));
                showProgress(false);
            }
        });
//        user.login(new StackMobCallback() {
//            @Override
//            public void success(String s) {
//                identityService.setUser(user);
//                Intent intent = new Intent(NewUserActivity.this, CrankyActivity.class);
//                startActivity(intent);
//                finish();
//            }
//
//            @Override
//            public void failure(StackMobException e) {
//                editTextUsername.setError(getString(org.riksa.cranky.R.string.username_already_in_use));
//            }
//        });
    }

    private void createUserWithFacebook() {
        final User user = new User(editTextUsername.getText().toString());
        user.createWithFacebook(identityService.getFacebookToken(), new StackMobCallback() {
            @Override
            public void success(String s) {
                identityService.setUser(user);
                Intent intent = new Intent(NewUserActivity.this, CrankyActivity.class);
                startActivity(intent);
                finish();
            }

            @Override
            public void failure(StackMobException e) {
                editTextUsername.setError(getString(org.riksa.cranky.R.string.username_already_in_use));
                showProgress(false);
            }
        });
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginStatusView.setVisibility(View.VISIBLE);
            mLoginStatusView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 1 : 0)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
                        }
                    });

            mLoginFormView.setVisibility(View.VISIBLE);
            mLoginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha(show ? 0 : 1)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                        }
                    });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}