package org.riksa.cranky;

import com.google.inject.AbstractModule;
import org.riksa.cranky.services.*;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/4/13
 */
public class CrankyModule extends AbstractModule {
    @Override
    protected void configure() {
//        if (BuildConfig.DEBUG) {
//            bind(CareerService.class).to(MockCareerService.class);
//            bind(IdentityService.class).to(MockIdentityService.class);
//            bind(StatisticsService.class).to(MockStatisticsService.class);
//        } else {
//            bind(CareerService.class).to(StackMobCareerService.class);
//            bind(IdentityService.class).to(LocalIdentityService.class);
//            bind(StatisticsService.class).to(StackMobStatisticsService.class);
//        }
        bind(CareerService.class).to(StackMobCareerService.class);
        bind(IdentityService.class).to(LocalIdentityService.class);
        bind(StatisticsService.class).to(StackMobStatisticsService.class);
        bind(HardwareService.class).to(HardwareServiceImpl.class);
    }
}
