package org.riksa.cranky.services;

import com.google.inject.Singleton;
import org.riksa.cranky.model.User;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/4/13
 */
@Singleton
public class LocalIdentityService implements IdentityService {
    User user;
    private String facebookToken;

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    @Override
    public String getFacebookToken() {
        return facebookToken;
    }
}
