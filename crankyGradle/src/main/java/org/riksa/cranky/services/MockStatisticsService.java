package org.riksa.cranky.services;

import org.riksa.cranky.model.User;

import java.util.Collections;
import java.util.List;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/20/13
 */
public class MockStatisticsService implements StatisticsService {
    @Override
    public void refresh() {
        refresh(null);
    }

    @Override
    public void refresh(AsyncCallback callback) {
        if (callback != null) {
            callback.success();
        }
    }

    @Override
    public List<User> getLeaderBoard() {
        return Collections.emptyList();
    }
}
