package org.riksa.cranky.services;

import org.riksa.cranky.model.User;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/20/13
 */
public class MockIdentityService implements IdentityService {
    User user;
    private String facebookToken;

    public MockIdentityService() {
        user = new User("Seppo");
        user.setScore(123);
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    @Override
    public String getFacebookToken() {
        return facebookToken;
    }

}
