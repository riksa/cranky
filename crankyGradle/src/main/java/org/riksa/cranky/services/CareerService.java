package org.riksa.cranky.services;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/4/13
 */
public interface CareerService {

    int getScore();

    void setScore(int score);
}
