package org.riksa.cranky.services;

import android.os.Vibrator;
import com.google.inject.Inject;
import org.riksa.cranky.R;
import roboguice.inject.InjectResource;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 1/12/14
 */
public class HardwareServiceImpl implements HardwareService {
    @Inject
    Vibrator vibrator;

    private double previousVibrateAngle = 0d;

    @InjectResource(R.integer.vibrate_angle)
    int vibrateAngle;

    @InjectResource(R.integer.vibrate_ms)
    int vibrateMs;

    @Override
    public void hapticVibrate(double angle) {
        double delta = Math.abs(previousVibrateAngle - angle);
        if (180d * delta / Math.PI > vibrateAngle) {
            previousVibrateAngle = angle;
            vibrator.vibrate(vibrateMs);
        }
    }
}
