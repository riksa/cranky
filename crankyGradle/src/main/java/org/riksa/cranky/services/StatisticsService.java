package org.riksa.cranky.services;

import org.riksa.cranky.model.User;

import java.util.List;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/18/13
 */
public interface StatisticsService {
    void refresh();
    void refresh(AsyncCallback callback);

    List<User> getLeaderBoard();
}
