package org.riksa.cranky.services;

import org.riksa.cranky.model.User;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/4/13
 */
public interface IdentityService {
    void setUser(User user);

    User getUser();

    void setFacebookToken(String facebookToken);

    String getFacebookToken();
}
