package org.riksa.cranky.services;

import com.google.inject.Singleton;
import com.stackmob.sdk.api.StackMobQuery;
import com.stackmob.sdk.callback.StackMobQueryCallback;
import com.stackmob.sdk.exception.StackMobException;
import org.riksa.cranky.model.User;

import java.util.LinkedList;
import java.util.List;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/18/13
 */
@Singleton
public class StackMobStatisticsService implements StatisticsService {
    private static final String FIELD_SCORE = "score";
    private static final Integer LIMIT = 10;
    private static final List<User> leaderBoard = new LinkedList<User>();

    @Override
    public void refresh() {
        refresh(null);
    }

    @Override
    public void refresh(final AsyncCallback callback) {
        User.query(User.class, new StackMobQuery().fieldIsOrderedBy(FIELD_SCORE, StackMobQuery.Ordering.DESCENDING).isInRange(0, LIMIT), new StackMobQueryCallback<User>() {
            @Override
            public void success(List<User> result) {
                synchronized (leaderBoard) {
                    leaderBoard.clear();
                    leaderBoard.addAll(result);
                }
                if (callback != null) {
                    callback.success();
                }
            }

            @Override
            public void failure(StackMobException e) {
                if (callback != null) {
                    callback.failure();
                }
            }
        });
    }

    @Override
    public List<User> getLeaderBoard() {
        return leaderBoard;
    }
}
