package org.riksa.cranky.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.stackmob.sdk.callback.StackMobModelCallback;
import com.stackmob.sdk.exception.StackMobException;
import org.riksa.cranky.model.User;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/4/13
 */
@Singleton
public class StackMobCareerService implements CareerService {

    @Inject
    IdentityService identityService;

    @Override
    public int getScore() {
        User user = identityService.getUser();
        return user.getScore();
    }

    @Override
    public void setScore(int score) {
        final User user = identityService.getUser();
        user.setScore(score);
        user.save(new StackMobModelCallback() {
            @Override
            public void success() {
                identityService.setUser(user);
            }

            @Override
            public void failure(StackMobException e) {

            }
        });

    }
}
