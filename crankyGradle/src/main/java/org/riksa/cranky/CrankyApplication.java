package org.riksa.cranky;

import android.app.Application;
import com.stackmob.android.sdk.common.StackMobAndroid;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/18/13
 */
public class CrankyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        StackMobAndroid.init(getApplicationContext(), 0, getString(R.string.stackmob_appkey));
    }
}
