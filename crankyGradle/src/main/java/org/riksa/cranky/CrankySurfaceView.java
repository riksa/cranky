package org.riksa.cranky;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import org.riksa.twirl.TwirlCallback;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/20/13
 */
public class CrankySurfaceView extends SurfaceView implements SurfaceHolder.Callback, TwirlCallback {
    private static final double RAD_TO_DEG = 180d / Math.PI;
    private float TARGET_SIZE = 50;
    private float TARGET_SCALE = 1f;
    private Bitmap crank;
    private Bitmap target;
    private final Matrix crankMatrix = new Matrix();
    private final Matrix targetMatrix = new Matrix();
    private boolean crankVisible = false;
    private float TARGET_X = 0;
    private float TARGET_Y = 0;
    private double prevAngle = 0;
    private double targetAngle = 0; // "Score"
    private final Path beltPath = new Path();
    private final Paint paint = new Paint();
    private final RectF rectC = new RectF();
    private final RectF rectT = new RectF();

    public CrankySurfaceView(Context context) {
        super(context);
        init();
    }

    public CrankySurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CrankySurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        crank = BitmapFactory.decodeResource(getResources(), R.drawable.crank);
        target = BitmapFactory.decodeResource(getResources(), R.drawable.target);
        getHolder().addCallback(this);
        setFocusable(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(25);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (crankVisible) {
            canvas.drawPath(beltPath, paint);
            canvas.drawBitmap(crank, crankMatrix, null);
        }
        canvas.drawBitmap(target, targetMatrix, null);

    }

    // SurfaceHolder.Callback
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    // SurfaceHolder.Callback
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        TARGET_SIZE = (float) getWidth() / 8f;
        TARGET_SCALE = TARGET_SIZE / (float) target.getWidth();
        TARGET_X = getWidth() / 2;
        TARGET_Y = TARGET_SIZE;
        targetMatrix.reset();
        targetMatrix.setTranslate(-target.getWidth() / 2, -target.getHeight() / 2);
        targetMatrix.postScale(TARGET_SCALE, TARGET_SCALE);
        targetMatrix.postTranslate(TARGET_X, TARGET_Y); // top center
    }

    // SurfaceHolder.Callback
    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    // TwirlCallback
    @Override
    public void twirlUpdate(int pointerId, Rect bounds, double handleDistance, double angle, int rotations, double localAngle) {
        float degrees = (float) (angle * RAD_TO_DEG);
        float scale = 2f * (float) handleDistance / crank.getWidth();
        targetAngle += (angle - prevAngle) * handleDistance / 400f;
        prevAngle = angle;

        // target
        updateTargetMatrix();

        // crank
        crankVisible = true;
        crankMatrix.reset();
        crankMatrix.setTranslate(-crank.getWidth() / 2, -crank.getHeight() / 2);
        crankMatrix.postRotate(degrees);
        crankMatrix.postScale(scale, scale);
        crankMatrix.postTranslate(bounds.centerX(), bounds.centerY());

        // calc chain start
        // vector from target center towards crank center
        float dy = (bounds.centerY() - TARGET_Y);
        float dx = (bounds.centerX() - TARGET_X);
        float dist = (float) Math.sqrt(dx * dx + dy * dy);
        // unit vector
        dx = dx / dist;
        dy = dy / dist;

        beltPath.reset();
        // perpendicular vector (x,y) -> (-y,x) & (y,-x)
        // start the path from the targets edge. magic 0.4 = 80% of radius (so that the belt is slightly inside of the wheel)
        beltPath.moveTo(TARGET_X + dy * TARGET_SIZE * 0.4f, TARGET_Y - dx * TARGET_SIZE * 0.4f);
        rectC.set(
                (float) (bounds.centerX() - handleDistance * 0.4f),
                (float) (bounds.centerY() - handleDistance * 0.4f),
                (float) (bounds.centerX() + handleDistance * 0.4f),
                (float) (bounds.centerY() + handleDistance * 0.4f));
        // atan2(dy,dx) gives the angle from center of target to center of crank.
        // swap the coefficients and sign of one to get the tangent.
        // now we have the angle from center of crank to where it intersects the crank rim.
        // draw 180 arc from there
        // since we started from the target, the first part of path is the segment from
        // targets rim to the cranks rim. and we continue with 180 arc from there.
        beltPath.arcTo(rectC, (float) (Math.atan2(-dx, dy) * RAD_TO_DEG), 180);

        // calculate similar bounding box for target.
        rectT.set(
                TARGET_X - TARGET_SIZE * 0.4f,
                TARGET_Y - TARGET_SIZE * 0.4f,
                TARGET_X + TARGET_SIZE * 0.4f,
                TARGET_Y + TARGET_SIZE * 0.4f);

        // draw the arc to the target. since we finished at the rim of target,
        // this part of path will implicitly draw the segment that connects crank
        // back to the target
        beltPath.arcTo(rectT, (float) (Math.atan2(dx, -dy) * RAD_TO_DEG), 180);

        invalidate();

    }

    private void updateTargetMatrix() {
        targetMatrix.reset();
        targetMatrix.setTranslate(-target.getWidth() / 2, -target.getHeight() / 2);
        targetMatrix.postRotate((float) (targetAngle * RAD_TO_DEG));
        targetMatrix.postScale(TARGET_SCALE, TARGET_SCALE);
        targetMatrix.postTranslate(TARGET_X, TARGET_Y); // top center
    }

    // TwirlCallback
    @Override
    public void twirlRemoved(int pointerId, double angle, int rotations, double localAngle) {
        crankVisible = false;
        invalidate();
    }

    // TwirlCallback
    @Override
    public void twirlAdded(int pointerId, double angle, int rotations, double localAngle) {
        prevAngle = angle;
    }
}
