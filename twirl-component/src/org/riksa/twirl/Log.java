package org.riksa.twirl;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/2/13
 */
public class Log {
    private static final String TAG = Log.class.getSimpleName();

    public static void d(String format, Object... params) {
        if (BuildConfig.DEBUG) {
            try {
                android.util.Log.d(TAG, String.format(format, params));
            } catch (Exception e) {
                Log.e(e.getMessage());
            }
        }
    }

    public static void e(String format, Object... params) {
        try {
            android.util.Log.e(TAG, String.format(format, params));
        } catch (Exception e) {
            Log.e(e.getMessage());
        }
    }
}
