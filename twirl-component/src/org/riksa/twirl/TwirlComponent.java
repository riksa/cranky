package org.riksa.twirl;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.AttributeSet;
import android.view.View;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/2/13
 */
public class TwirlComponent extends View {
    private static final String TAG = TwirlComponent.class.getSimpleName();
    private Drawable knobDrawable;
    private Drawable markerDrawable;
    private Drawable boundsDrawable;
    private boolean debug = false;
    private double angle = 0;

    private double markerScale = 0.2d;
    private double markerDist = 0.8d;

    private Rect bounds = new Rect(0, 0, 150, 150);
    private Rect markerBounds = new Rect(10, 10, 50, 50);

    public TwirlComponent(Context context) {
        super(context);
        init(context);
    }

    public TwirlComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TwirlComponent(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        Resources resources = context.getResources();
        setKnobDrawable(resources.getDrawable(R.drawable.default_knob));
        setMarkerDrawable(resources.getDrawable(R.drawable.default_marker));
        if (debug) {
            boundsDrawable = new ShapeDrawable(new RectShape());
            boundsDrawable.setAlpha(127);
        }
        updateMarkerBounds();
    }

    private void updateMarkerBounds() {
        double rad = bounds.width() / 2d * markerDist;
        double w = bounds.width() * markerScale;
        double h = bounds.height() * markerScale;
        double cx = bounds.exactCenterX() + Math.cos(angle) * rad;
        double cy = bounds.exactCenterY() + Math.sin(angle) * rad;

        markerBounds.set(0, 0, (int) w, (int) h);
        markerBounds.offsetTo((int) (cx - w / 2d), (int) (cy - h / 2d));

        //To change body of created methods use File | Settings | File Templates.
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (debug) {
            boundsDrawable.draw(canvas);
        }
        knobDrawable.draw(canvas);
        markerDrawable.draw(canvas);
    }

    public void setKnobDrawable(Drawable knobDrawable) {
        this.knobDrawable = knobDrawable;
    }

    public void setMarkerDrawable(Drawable markerDrawable) {
        this.markerDrawable = markerDrawable;
    }

    public void update(TwirlController twirlController) {
        bounds = twirlController.getBounds();
        angle = twirlController.getAngle();
        updateMarkerBounds();

        knobDrawable.setBounds(bounds);
        markerDrawable.setBounds(markerBounds);
        if (debug) {
            boundsDrawable.setBounds(twirlController.getBoundingBox());
        }
        invalidate();
    }

}
