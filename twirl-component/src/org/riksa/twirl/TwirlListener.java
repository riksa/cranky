package org.riksa.twirl;

import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * TODO: Oneliner
 * <p/>
 * TODO: Description
 * <p/>
 *
 * @author riksa
 * @version 1.0
 * @since 11/2/13
 */
public class TwirlListener implements View.OnTouchListener {
    private final ViewGroup viewGroup;
    private boolean drawTwirlers = false;

    private TwirlCallback twirlCallback;

    final Map<Integer, TwirlController> twirlControllers = new HashMap<Integer, TwirlController>();
    final Map<Integer, TwirlComponent> twirlComponents = new HashMap<Integer, TwirlComponent>();
    final List<TwirlComponent> twirlPool = new LinkedList<TwirlComponent>();

    public TwirlListener(ViewGroup viewGroup) {
        this.viewGroup = viewGroup;
        viewGroup.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        final int action = event.getAction();

        switch (action & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: {
                final int pointerIndex = 0;
                final int pointerId = event.getPointerId(pointerIndex);
                final int pointerX = (int) event.getX();
                final int pointerY = (int) event.getY();
                addTwirler(pointerId, pointerX, pointerY);
                return true;
            }
            case MotionEvent.ACTION_POINTER_DOWN: {
                final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                final int pointerId = event.getPointerId(pointerIndex);
                final int pointerX = (int) event.getX(pointerIndex);
                final int pointerY = (int) event.getY(pointerIndex);
                addTwirler(pointerId, pointerX, pointerY);
                return true;
            }
            case MotionEvent.ACTION_MOVE: {
                for (int pointerIndex = 0; pointerIndex < event.getPointerCount(); pointerIndex++) {
                    final int pointerId = event.getPointerId(pointerIndex);
                    TwirlController twirlController = getTwirlController(pointerId);
                    final int pointerX = (int) event.getX(pointerIndex);
                    final int pointerY = (int) event.getY(pointerIndex);
                    updateTwirler(pointerId, twirlController, pointerX, pointerY);
                }
                return true;
            }
            case MotionEvent.ACTION_UP: {
                final int pointerIndex = 0;
                final int pointerId = event.getPointerId(pointerIndex);
                removeTwirler(pointerId);
                return true;

            }
            case MotionEvent.ACTION_POINTER_UP: {
                final int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                final int pointerId = event.getPointerId(pointerIndex);
                removeTwirler(pointerId);
                return true;
            }
        }

        return false;
    }

    private void updateTwirler(int pointerId, TwirlController twirlController, int pointerX, int pointerY) {
        twirlController.registerLocation(pointerX, pointerY);
        updateTwirlComponent(getTwirlComponent(pointerId), twirlController);
        twirlCallback.twirlUpdate(pointerId, twirlController.getBoundingBox(), twirlController.getHandleDistance(), twirlController.getAngle(), twirlController.getRotations(), twirlController.getLocalAngle());
    }

    private void removeTwirler(int pointerId) {
        TwirlController twirlController = getTwirlController(pointerId);
        twirlController.stop();
        hideTwirlComponent(pointerId);
        twirlCallback.twirlRemoved(pointerId, twirlController.getAngle(), twirlController.getRotations(), twirlController.getLocalAngle());
    }

    private void addTwirler(int pointerId, int pointerX, int pointerY) {
        TwirlController twirlController = getTwirlController(pointerId);
        twirlController.start(pointerX, pointerY);
        updateTwirlComponent(showTwirlComponent(pointerId), twirlController);
        twirlCallback.twirlAdded(pointerId, twirlController.getAngle(), twirlController.getRotations(), twirlController.getLocalAngle());
    }

    private TwirlController getTwirlController(int pointerId) {
        synchronized (twirlControllers) {
            if (!twirlControllers.containsKey(pointerId)) {
                twirlControllers.put(pointerId, new TwirlController());
            }
            return twirlControllers.get(pointerId);
        }
    }

    private void updateTwirlComponent(TwirlComponent twirlComponent, TwirlController twirlController) {
        if (twirlComponent != null && drawTwirlers ) {
            twirlComponent.update(twirlController);
        }
    }

    private TwirlComponent showTwirlComponent(int pointerId) {
        // TODO: pool
        TwirlComponent twirlComponent = getTwirlComponent(pointerId);
        if (twirlComponent != null && drawTwirlers ) {
            twirlComponent.setVisibility(View.VISIBLE);
            twirlComponent.update(getTwirlController(pointerId));
        }
        return twirlComponent;
    }

    private TwirlComponent getTwirlComponent(int pointerId) {
        synchronized (twirlComponents) {
            if (!twirlComponents.containsKey(pointerId)) {
                TwirlComponent twirlComponent;
                synchronized (twirlPool) {
                    if (twirlPool.size() > 0) {
                        twirlComponent = twirlPool.remove(0);
                    } else {
                        twirlComponent = new TwirlComponent(viewGroup.getContext());
                        viewGroup.addView(twirlComponent);
                    }
                    twirlComponents.put(pointerId, twirlComponent);
                }
            }
            return twirlComponents.get(pointerId);
        }
    }

    private void releaseTwirlComponent(int pointerId) {
        TwirlComponent removed;
        synchronized (twirlComponents) {
            removed = twirlComponents.remove(pointerId);
        }

        synchronized (twirlPool) {
            twirlPool.add(removed);
        }
    }


    private void hideTwirlComponent(int pointerId) {
        TwirlComponent twirlComponent = getTwirlComponent(pointerId);
        if (twirlComponent != null) {
            twirlComponent.setVisibility(View.INVISIBLE);
            releaseTwirlComponent(pointerId);
        }
    }

    public void setTwirlCallback(TwirlCallback twirlCallback) {
        this.twirlCallback = twirlCallback;
    }

    public void setDrawTwirlers(boolean drawTwirlers) {
        this.drawTwirlers = drawTwirlers;
    }
}
